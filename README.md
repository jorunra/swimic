# SWIMIC #

A sequence alignment tool utilizing the
Smith-Waterman algorithm implemented for Intel’s MIC (Many Integrated
Core) architecture. It runs natively on a Xeon Phi coprocessor and
is optimized with SIMD intrinsics, threading with OpenMP and pragma
directives for vectorization.

SWIMIC is created as a part of my master study at The University of Oslo at the
Department of Informatics.

### Sumary ###

* Required software
* Preparation of Database
* Build and run on CPU
* Build and run on MIC architecture

### Required software ###

Intel® Parallel Studio XE's *icc* compiler is required to build SWIMIC for MIC architecture.

### Preparation of Database ###

SWIMIC requires a preprocessed FASTA format database. To prepare the database
use the obj/prepare.o file with the database in FASTA format as the only
parameter.

    ./obj/prepare.o <database_fasta>

It is also possible to make a new objectfile with

    make prepare

### Build and run on CPU ###

Either use the object file provided in the obj/ folder or create new with

    make

or

    make debug

for debug output.

To run with default parameter use

    ./obj/swimic.o -d <preprocessed_database> -q <query_fasta>

use

    ./obj/swimic.o

or

    ./obj/swimic.o -h

for help and a list of optional parameter.

### Build and run on MIC architecture ###

Either use the object file provided in the obj/ folder or create new with

    make mic

or

    make mic_debug

for debug output.

To run with default parameter use

    ./obj/swimic.mic -d <preprocessed_database> -q <query_fasta>

use

    ./obj/swimic.mic

or

    ./obj/swimic.mic -h

for help and a list of optional parameter.


### Contact ###

* jorunra@ifi.uio.no