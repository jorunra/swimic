#
#  There exist several targets which are by default empty and which can be 
#  used for execution of your targets. These targets are usually executed 
#  before and after some main targets. They are: 
#
#     .build-pre:              called before 'build' target
#     .build-post:             called after 'build' target
#     .clean-pre:              called before 'clean' target
#     .clean-post:             called after 'clean' target
#     .clobber-pre:            called before 'clobber' target
#     .clobber-post:           called after 'clobber' target
#     .all-pre:                called before 'all' target
#     .all-post:               called after 'all' target
#     .help-pre:               called before 'help' target
#     .help-post:              called after 'help' target
#
#  Targets beginning with '.' are not intended to be called on their own.
#
#  Main targets can be executed directly, and they are:
#  
#     build                    build a specific configuration
#     clean                    remove built files from a configuration
#     clobber                  remove all built files
#     all                      build all configurations
#     help                     print help mesage
#  
#  Targets .build-impl, .clean-impl, .clobber-impl, .all-impl, and
#  .help-impl are implemented in nbproject/makefile-impl.mk.
#
#  Available make variables:
#
#     CND_BASEDIR                base directory for relative paths
#     CND_DISTDIR                default top distribution directory (build artifacts)
#     CND_BUILDDIR               default top build directory (object files, ...)
#     CONF                       name of current configuration
#     CND_PLATFORM_${CONF}       platform name (current configuration)
#     CND_ARTIFACT_DIR_${CONF}   directory of build artifact (current configuration)
#     CND_ARTIFACT_NAME_${CONF}  name of build artifact (current configuration)
#     CND_ARTIFACT_PATH_${CONF}  path to build artifact (current configuration)
#     CND_PACKAGE_DIR_${CONF}    directory of package (current configuration)
#     CND_PACKAGE_NAME_${CONF}   name of package (current configuration)
#     CND_PACKAGE_PATH_${CONF}   path to package (current configuration)
#
# NOCDDL


# Environment
MKDIR=mkdir
CP=cp
CCADMIN=CCadmin

INC_DIR = inc/
SRC_DIR = src/

GCC=gcc
CFLAGS= -g -Wall -O3 -fopenmp -msse4.1 -I$(INC_DIR)

FILES = $(SRC_DIR)*.c
TARGET = swimic

CC=icc
PFLAGS= -g -Wall -O2 -openmp -mmic -I$(INC_DIR) #-vec-report6

TARGET_P = swimic_phi
TARGET_A = swimic_abel

PRE_DIR = pre/
PRE_FLAG =  -I$(PRE_DIR)
PRE_FILES = $(PRE_DIR)*.c $(SRC_DIR)read.c
PRE_TARGET = prepare




all:
	$(GCC) $(CFLAGS) -o $(TARGET).o $(FILES)            

debug:
	$(GCC) $(CFLAGS) -DDEBUG -o $(TARGET)_debug.o $(FILES)

prepare:
	$(GCC) $(CFLAGS) $(PREFLAG) -o $(PRE_TARGET).o $(PRE_FILES)

mic:
	$(CC) $(PFLAGS) -DPHI_COMPILED -o $(TARGET).mic $(FILES)

mic_debug:
	$(CC) $(PFLAGS) -DPHI_COMPILED -DDEBUG -o $(TARGET)_debug.mic $(FILES)




phi:
	$(CC) $(PFLAGS) -DPHI_COMPILED -o $(TARGET_P).mic $(SERIAL) $(FILES)

phi_debug:
	$(CC) $(PFLAGS) -DPHI_COMPILED -DDEBUG -o $(TARGET_P)_debug.mic $(FILES)

abel:
	$(CC) $(PFLAGS) -DPHI_COMPILED -o $(TARGET_A).mic $(FILES)              

abel_debug:
	$(CC) $(PFLAGS) -DPHI_COMPILED -DDEBUG -o $(TARGET_A)_debug.mic $(FILES)

clean:
	rm -rf *.mic *.o                                                        



