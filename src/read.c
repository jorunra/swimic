/* 
 * File:   read.c
 * Author: Jorun Ramstad
 *
 * Created on July 26, 2015, 5:10 PM
 *
 * Read the processed sequence from file
 *
 *
 * @param FILE *file            The open file to read the sequence from
 * @param char **db             The alloceted space for all sequences
 * */
#include "common.h"

long read_pdb(FILE *file, char **db, idx_t **idx, long *residues) {
    int len, k, rv;
    long seq_total, len_total;
    rv = fread((void *)&len_total, sizeof(long), 1, file);
    rv |= fread((void *)residues, sizeof(long), 1, file);
    rv |= fread((void *)&seq_total, sizeof(long), 1, file);

#ifdef PHI_COMPILED
    *idx = (idx_t *)_mm_malloc(seq_total*sizeof(idx_t),64);
    *db = (char *)malloc(((len_total)+1)*sizeof(char));
#else
    *idx = (idx_t *)malloc(seq_total*sizeof(idx_t));
    *db = (char *)malloc(((len_total)+1)*sizeof(char));
#endif

    char *dbp = *db;

    for (k = 0; k < seq_total; k++) {
        rv |= fread((void *)&len, sizeof(int), 1, file);
        rv |= fread((void *)dbp, sizeof(char), len, file);
        rv |= fread((void *)(*idx)[k].description, sizeof(char), DES_LEN, file);
        (*idx)[k].index = dbp - *db;
        (*idx)[k].length = len;
        (*idx)[k].score = 0;
        dbp += len;
    }

    /*
       int i;
       for (k = 0; k < seq_total; k++) {
       for (i = 0; i < (*idx)[k].length; i++)
       printf("%c ",sym_map[(int)(*db)[(*idx)[k].index + i]]);
       printf(" %d\n", (*idx)[k].length);
       }
       */
    //printf("rv = %d\n",rv);
    //if (rv != EOF)
    //    return -1;
    return seq_total;
}
long read_distributed_pdb(FILE *file, char **db, idx_t **idx, long *residues, long *len_total) {
    int len, align, k, i, rv;
    long seq_total;
    rv = fread((void *)len_total, sizeof(long), 1, file);
    rv |= fread((void *)residues, sizeof(long), 1, file);
    rv |= fread((void *)&seq_total, sizeof(long), 1, file);
    rv |= fread((void *)&distribution, sizeof(int), 1, file);

#ifdef debug
    printf("distribution: %d\n", distribution);
#endif

#ifdef PHI_COMPILED
    *idx = (idx_t *)_mm_malloc(seq_total*sizeof(idx_t),64);
    *db = (char *)malloc(((*len_total)+1)*sizeof(char));
#else
    *idx = (idx_t *)malloc(seq_total*sizeof(idx_t));
    *db = (char *)malloc(((*len_total)+1)*sizeof(char));
#endif

    char *dbp = *db;

    for (k = 0; k <= seq_total-distribution; k += distribution) {
        rv |= fread((void *)&align, sizeof(int), 1, file);
        rv |= fread((void *)&len, sizeof(int), 1, file);
        //printf("%d | %d \n", align , len);

        rv |= fread((void *)dbp, sizeof(char), align*distribution, file);

        for (i = 0; i < distribution; i++) {
            rv |= fread((void *)(*idx)[k+i].description, sizeof(char), DES_LEN, file);
            (*idx)[k+i].index = (dbp - *db) + i;

            (*idx)[k+i].length = len;
            (*idx)[k+i].score = 0;
        }

        dbp += align*distribution;
    }

    int rest = seq_total % distribution;
#ifdef debug
    printf("rest: %d\n", rest);
#endif
    for (i = rest; i > 0; i--) {
        rv |= fread((void *)&len, sizeof(int), 1, file);
        rv |= fread((void *)dbp, sizeof(char), len, file);
        rv |= fread((void *)(*idx)[seq_total-i].description, sizeof(char), DES_LEN, file);
        (*idx)[seq_total-i].index = (dbp - *db);
        (*idx)[seq_total-i].length = len;
        (*idx)[seq_total-i].score = 0;
        dbp += len;
    }

    /*
    int j;
    for (k = 0; k <= seq_total-distribution; k += distribution) {

        for (j = 0; j < distribution; j++) {
            for (i = 0; i < (*idx)[k].length; i++) {
                printf("%c ",sym_map[(int)(*db)[(*idx)[k+j].index + (i*distribution)]]);
            }
            printf(" %d\n", (*idx)[k+j].length);
        }

    }
    for (i = rest; i > 0; i--) {
        for (j = 0; j < (*idx)[seq_total - i].length; j++) {
            printf("%c ",sym_map[(int)(*db)[(*idx)[seq_total-i].index + j]]);
        }
        printf(" %d\n", (*idx)[seq_total-i].length);
    }
    */
    //printf("rv = %d\n",rv);
    //if (rv != EOF)
    //    return -1;

    return seq_total;
}
int read_query (struct sequence *s, FILE *file) {
    int size = LINE_MAX;
    int c, s_len;
    char query[LINE_MAX];

    int first = 1;

    while (1) {
        // Read the description, return if EOF is reached
        if (fgets(query, LINE_MAX, file) == 0)
            return 0;

        s_len = strlen(query);

        // If the description is longer than LINE_MAX, the rest of the
        // description is read until a new line is reached.
        if (query[0] == '>') {
            if ((s_len == LINE_MAX - 1)){
                while (query[s_len - 1] != '\n') {
                    if(fgets(query, LINE_MAX, file) == 0)
                        return 0;
                    s_len = strlen(query);
                }
            }
            if (!first)
                return 1;
            first = 0;
        } else {
            // Read the sequence and store it in the given sequence
            // struct. Each amino acid is mapped to a number between 0-27.
            // Realloc if the sequence is too long.
            char *p = query;
            c = 0;
            while(c < s_len - 1) {
                if(s->len + 1 >= size) {
                    size += LINE_MAX;
                    s->seq = (char *) realloc(s->seq, size);
                }
                s->seq[s->len++] = map[(int) *p++];
                c++;
            }
        }
    }
    return 0;
}
