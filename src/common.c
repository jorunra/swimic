/* 
 * File:   common.c
 * Author: Jorun Ramstad
 *
 * Created on July 26, 2015, 5:09 PM
 */

#include "common.h"

const char map[256] = {
-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 25, -1, -1, 0, -1, -1,
-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
-1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 27, 10, 11, 12, 13, 26,
14, 15, 16, 17, 18, 24, 19, 20, 21, 22, 23, -1, -1, -1, -1, -1,
-1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 27, 10, 11, 12, 13, 26,
14, 15, 16, 17, 18, 24, 19, 20, 21, 22, 23, -1, -1, -1, -1, -1,
-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
};

const char * sym_map = "-ABCDEFGHIKLMNPQRSTVWXYZU*OJ####";

void print_sequence(struct sequence *s) {
    int c;
    for (c = 0; c < s->len;c++) {
        if (c % 80 == 0)
            printf("\n");
        printf("%c",sym_map[(int)s->seq[c]]);
    }
    printf("\n\n");
}

void write_sequence(struct sequence *s, FILE * file) {
    int c;
    for (c = 0; c < s->len; c++) {
        if((c % 80) == 0)
            fprintf(file, "\n");
        fprintf(file, "%c",sym_map[(int)s->seq[c]]);
    }
    fprintf(file,"\n\n");
}

void print_seq(char *seq, int len) {
    int c;
    for (c = 0; c < len;c++)
        printf("%c ",sym_map[(int)seq[c]]);
    printf("\n");
}
void write_matches(res_t *r, storage_t *high, FILE *file, idx_t *idx, int num) {
    if (high == NULL)
        return;

    fprintf(file,"%d: Sequence aligned with score %d\n", num, high->score);
    fprintf(file,"Description: %-30s ...\n\n", idx[high->db_index].description);
#ifdef DEBUG
    printf("%d: Sequence aligned with score %d\n", num, high->score);
    printf("Description: %-30s ...\n\n", idx[high->db_index].description);
#endif
    int i, j, stride = 70, o = 0;
    int c = strlen(r[high->s_idx].q);

    for (j = c; j >= 0; j -= 70) {
        if (j < 70)
            stride = j;
        o = 0;
        fprintf(file,"DB:  ");
        for (i = j-1; o < stride; i--, o++)
            fprintf(file,"%c", r[high->s_idx].m[i]);
        fprintf(file,"\n");

        o = 0;
        fprintf(file,"SYM: ");
        for (i = j-1; o < stride; i--, o++) {
            fprintf(file,"%c", r[high->s_idx].sym[i]);
        }
        fprintf(file,"\n");

        o = 0;
        fprintf(file,"Q:   ");
        for (i = j-1; o < stride; i--, o++)
            fprintf(file,"%c", r[high->s_idx].q[i]);
        fprintf(file,"\n\n");
#ifdef DEBUG
        o = 0;
        printf("DB:  ");
        for (i = j-1; o < stride; i--, o++)
            printf("%c", r[high->s_idx].m[i]);
        printf("\n");

        o = 0;
        printf("SYM: ");
        for (i = j-1; o < stride; i--, o++)
            printf("%c", r[high->s_idx].sym[i]);
        printf("\n");

        o = 0;
        printf("Q:   ");
        for (i = j-1; o < stride; i--, o++)
            printf("%c", r[high->s_idx].q[i]);
        printf("\n\n");
#endif
    }
#ifdef DEBUG
    printf("\n\n");
#endif
    fprintf(file,"\n\n");
    write_matches(r, high->next, file, idx, ++num);
}

storage_t *fill(storage_t *high, storage_t *cur, storage_t **low) {
    storage_t *tmp = high;
    while(1) {
        if (tmp->score >= cur->score) {
            if (tmp->next == NULL) {
                tmp->next = cur;
                cur->prev = tmp;
                (*low) = cur;
                break;
            }
            tmp = tmp->next;
        } else {
            if (tmp->prev == NULL) {
                cur->next = tmp;
                tmp->prev = cur;
                return cur;
            }
            tmp->prev->next = cur;
            cur->next = tmp;
            cur->prev = tmp->prev;
            tmp->prev = cur;
            break;
        }
    }
    return high;
}

storage_t *add(storage_t *storage_space, storage_t *high, storage_t **low, int *storage_idx, int score, long db_index) {
    storage_t *cur;

    if ((*storage_idx) == max_store) {
        if (score < (*low)->score)
            return high;
        cur = *low;
        (*low) = (*low)->prev;
        (*low)->next = NULL;
    } else {
        cur = &(storage_space[(*storage_idx)]);
        cur->s_idx = (*storage_idx)++;
    }

    cur->score = score;
    cur->db_index = db_index;
    cur->next = NULL;
    cur->prev = NULL;

    if ((*storage_idx) == 1){
        (*low) = cur;
        high = cur;
        return cur;
    }

    return fill(high, cur, low);
}

void print_matches(storage_t *cur, idx_t *idx) {
    if (cur == NULL)
        return;
    printf("Sequence %ld with score %d\n", cur->db_index, cur->score);
    print_matches(cur->next, idx);
}
