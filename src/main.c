/* 
 * File:   main.c
 * Author: Jorun Ramstad
 *
 * Created on July 26, 2015, 4:52 PM
 */

#include "common.h"
#include "scoring_matrix.h"

#include <sys/time.h>
#include <time.h>
#include <getopt.h>
#include <omp.h>

extern int optind;
extern char *optarg;
char *progname;

void print_help() {
    printf("Usage: %s [OPTIONS]\n", progname);
    printf("  -h, --help                 show help\n");
    printf("  -d, --db=FILE              sequence database filename (required)\n");
    printf("  -q, --query=FILE           query sequence filename (required)\n");
    printf("  -s, --matrix=NAME/FILE     score matrix name or filename (BLOSUM62)\n");
    printf("  -t, --threads              number of threads (120)\n");
    printf("  -r, --range                number of columns calculated per iteration (10)\n");
    printf("  -a, --affinity             the affinity used by OpenMP (balanced)\n");
    printf("  -m, --matches              number stored matches (10)\n");
}

int main (int argc, char *argv[]) {
    FILE *db_file = NULL, *q_file = NULL, *o_file = NULL;
    char *scoring_matrix = "blosum62";
    char *out = "serial.out";
    threads = 240;
    max_store = 10;
    int c;
    char *affinity = NULL;
    int range = 10;

    progname = argv[0];
    char *q_name = NULL, *db_name = NULL;

    if (argc == 1) {
        print_help();
        return -1;
    }

    while ((c = getopt(argc, argv, "q:d:o:s:t:r:m:a:")) != -1)
    {
        switch (c)
        {
            case 'q':
                q_file = fopen(optarg,"r");
                q_name = optarg;
                break;
            case 'd':
                db_file = fopen(optarg,"r");
                db_name = optarg;
                break;
            case 's':
                scoring_matrix = optarg;
                break;
            case 'o':
                out = optarg;
                break;
            case 't':
                threads = atoi(optarg);
                break;
            case 'r':
                range = atoi(optarg);
                break;
            case 'm':
                max_store = atoi(optarg);
                break;
            case 'a':
                affinity = optarg;
                break;

            default:
                print_help();
                break;
        }
    }

    if (db_file == NULL || q_file == NULL) {
        print_help();
        return -1;
    }

    matrix_init_buildin(scoring_matrix);
    //matrix_print();

#ifdef PHI_COMPILED
    struct sequence *q = (struct sequence *) _mm_malloc(sizeof(struct sequence),64);
    q->seq = (char *) malloc(LINE_MAX);
#else
    struct sequence *q = (struct sequence *) malloc(sizeof(struct sequence));
    q->seq = (char *) malloc(LINE_MAX);
#endif
    q->len = 0;

    char *db;
    idx_t *idx;
    long char_total, pading;
    int seq_total = read_distributed_pdb(db_file, &db, &idx, &char_total, &pading);

    read_query(q, q_file);

    int k;
#ifdef DEBUG
    printf("Database size: %ld residues in %d sequences\n", char_total, seq_total);
    printf("Size including pading : %ld \n", pading);
    printf("Query length : %ld \n", q->len);
    printf("Description: %30s\n", idx[169432].description);
#endif


#ifdef PHI_COMPILED
    storage_t *storage_space = (storage_t *) _mm_malloc(max_store*sizeof(storage_t),64);
#else
    storage_t *storage_space = (storage_t *) malloc(max_store*sizeof(storage_t));
#endif
    storage_t *high = NULL;
    storage_t *low = NULL;
    int storage_idx = 0;

    struct timeval t0;
    struct timeval t1;

    int rest = 0;

    omp_set_num_threads(threads);
#ifdef PHI_COMPILED
    if (affinity == NULL)
        kmp_set_defaults("KMP_AFFINITY=balanced");
    else
        kmp_set_defaults(affinity);
#endif

    gettimeofday(&t0, 0);

    rest = SIMD_ALIGN(range,q->seq, q->len, db, idx, score_matrix, seq_total);


#pragma omp parallel for schedule(dynamic)
    for (k = 0; k < rest; k++){
        long hearray[2 * q->len];
        idx[seq_total - k].score = (int)sw(hearray, q->seq, q->len, db, idx, (seq_total-k), score_matrix, 1);
    }

    for (k = 0; k < seq_total; k++) {
        high = add(storage_space, high, &low, &storage_idx, (int) idx[k].score, k);
    }
    gettimeofday(&t1, 0);
    double elapsed = ((double)t1.tv_sec + (double)t1.tv_usec/1000000) - ((double)t0.tv_sec + (double)t0.tv_usec/1000000);

    double gcups = ((q->len * char_total)/elapsed)/1000000000;
    //double gcups = ((q->len * pading)/elapsed)/1000000000;

    printf("%f\n", gcups);
    //printf("%f %d\n", gcups, q->len);

#ifdef DEBUG
    printf("Rest: %d\n", rest);
    print_sequence(q);
    printf("The best matching sequence with the score %d is sequens nr %ld: %30s\n", high->score, high->db_index, idx[high->db_index].description);
    printf("The time to align %d database sequences is %f s\n", seq_total, elapsed);
    print_matches(high, idx);
#endif


#ifdef PHI_COMPILED
    res_t *r = (res_t *) _mm_malloc(max_store*sizeof(res_t),64);
    struct match *m = (struct match *) _mm_malloc(max_store*sizeof(struct match),64);
#else
    res_t *r = (res_t *) malloc(max_store*sizeof(res_t));
    struct match *m = (struct match *) malloc(max_store*sizeof(struct match));
#endif




    #pragma omp parallel for schedule(dynamic)
    for (k = 0; k < storage_idx; k++) {
        int index = (storage_space[k].db_index);
#ifdef PHI_COMPILED
        r[k].q = (char *) _mm_malloc(q->len * 2, 64);
        r[k].m = (char *) _mm_malloc(q->len * 2, 64);
        r[k].sym = (char *) _mm_malloc(q->len * 2, 64);
        m[k].path = (long *) _mm_malloc((q->len+1)*(idx[index].length+1)*sizeof(long), 64);
        m[k].seq = (char *) _mm_malloc(idx[index].length, 64);
#else
        r[k].q = (char *) malloc(q->len * 2);
        r[k].m = (char *) malloc(q->len * 2);
        r[k].sym = (char *) malloc(q->len * 2);
        m[k].path = (long *) malloc((q->len+1)*(idx[index].length+1)*sizeof(long));
        m[k].seq = (char *) malloc(idx[index].length);
#endif
        m[k].len = idx[index].length;
        m[k].score = storage_space[k].score;


        if (index > seq_total-rest) {
            smith_waterman_distibuted(&m[k], q->seq, q->len, db, idx, index, score_matrix, 1);
        }
        else {
            smith_waterman_distibuted(&m[k], q->seq, q->len, db, idx, index, score_matrix, distribution);
        }
        pathfinder(q, &m[k], &r[k], k);
    }
    // Open and write the output file
    o_file = fopen(out, "w");


    fprintf(o_file, "Database size    : %ld residues in %d sequences\n", char_total, seq_total);
    fprintf(o_file, "Query file name  : %s\n", q_name);
    fprintf(o_file, "Query lengt      : %ld\n", q->len);
    fprintf(o_file, "Score matrix     : %s\n", scoring_matrix);
    fprintf(o_file, "Gap penalty      : %d + %d\n",GAPOPENEXTEND-1, GAPEXTEND);
    fprintf(o_file, "Max matches      : %d\n", max_store);
    fprintf(o_file, "Threads          : %d\n", threads);
    fprintf(o_file, "Elapsed          : %f s\n", elapsed);
    fprintf(o_file, "GCUPS            : %f\n\n\n", gcups);


    fprintf(o_file, "Query sequence   :");
    write_sequence(q, o_file);

    write_matches(r, high, o_file, idx, 1);

    // Free up memory and close files
#pragma omp parallel for schedule(dynamic)
    for (k = 0; k < storage_idx; k++) {
#ifdef PHI_COMPILED
        _mm_free(r[k].q);
        _mm_free(r[k].m);
        _mm_free(r[k].sym);
        _mm_free(m[k].path);
#else
        free(r[k].q);
        free(r[k].m);
        free(r[k].sym);
        free(m[k].path);
#endif
    }
    free(q->seq);

#ifdef PHI_COMPILED
    _mm_free(r);
    _mm_free(m);

    _mm_free(storage_space);
    _mm_free(q);
    free(db);
    _mm_free(idx);
#else
    free(r);
    free(m);

    free(storage_space);
    free(q);
    free(db);
    free(idx);
#endif


    fclose(o_file);
    fclose(q_file);
    fclose(db_file);

    return (EXIT_SUCCESS);
}
