/* 
 * File:   align_serial.c
 * Author: Jorun Ramstad
 *
 * Created on July 26, 2015, 6:12 PM
 */
#include "common.h"

long fullsw(char *dseq, char *dend, char *qseq, char *qend, long *hearray, int16_t *score_matrix, int gapopenextend, int gapextend) {
    long h, n, e, f, s;
    long *hep;
    char *qp, *dp;
    int16_t *sp;

    s = 0;
    dp = dseq;
    memset(hearray, 0, 2 * sizeof(long) * (qend-qseq));

    while (dp < dend)
    {
        f = 0;
        h = 0;
        hep = hearray;
        qp = qseq;
        sp = score_matrix + (*dp << 5);

        while (qp < qend)
        {
            n = *hep;
            e = *(hep+1);
            h += sp[(int)(*qp)];

            if (e > h)
                h = e;
            if (f > h)
                h = f;
            if (h < 0)
                h = 0;
            if (h > s)
                s = h;

            *hep = h;
            e -= gapextend;
            f -= gapextend;
            h -= gapopenextend;

            if (h > e)
                e = h;
            if (h > f)
                f = h;

            *(hep+1) = e;
            h = n;
            hep += 2;
            qp++;
        }

        dp++;
    }

    return s;
}



long smith_waterman_distibuted (struct match *m, char *query, int q_len, char *db, idx_t *idx, int k, int16_t *score_matrix, int d) {
    int i, j, i_idx = -1, j_idx = -1;
    long score = 0, h, e , f;

#ifdef PHI_COMPILED
    long *earray = (long *) _mm_malloc(q_len*sizeof(long),64);
#else
    long *earray = (long *) malloc(q_len*sizeof(long));
#endif
    memset(m->path, 0,  sizeof(long) * ((q_len+1) * (m->len+1)));
    memset(earray, 0,  sizeof(long) * (q_len));

    int first = 1;

    for (i = 0; i < idx[k].length; i++) {
        f = 0;
        h = 0;
        m->seq[i] = db[idx[k].index + i*d];

        for (j = 0; j < q_len; j++) {

            e = earray[j];

            h += score_matrix[query[j]*32 + db[idx[k].index + i*d]];


            if (e > h)
                h = e;
            if (f > h)
                h = f;
            if (h < 0)
                h = 0;
            if (h > score) {
                score = h;
                i_idx = i;
                j_idx = j;
            }

            m->path[j*idx[k].length + i] = h;

            e -= GAPEXTEND;
            f -= GAPEXTEND;
            h -= GAPOPENEXTEND;

            if (h > e)
                e = h;
            if (h > f)
                f = h;

            earray[j] = e;

            if (first) {
                first = 0;
                h = m->path[j*idx[k].length + i];
            } else
                h = m->path[j*idx[k].length + i-1];


        }
    }

    if ((int)idx[k].score != (int)score) {
        printf("Error! %4d | %d\n", (int)idx[k].score, (int)score);
        m->path[j_idx*idx[k].length + i_idx] = idx[k].score;
    }
#ifdef DEBUG
    else {
        printf("Match! %4d | %d\n", (int)idx[k].score, (int)score);
    }
#endif

#ifdef PHI_COMPILED
    _mm_free(earray);
#else
    free(earray);
#endif
    return score;

}
long sw (long *hearray, char *query, int q_len, char *db, idx_t *idx, int k, int16_t *score_matrix, int d) {
    int i, j;


    long score = 0, h, e , f, n;

    memset(hearray, 0,  sizeof(long) * 2 * (q_len));

    for (i = 0; i < idx[k].length; i++) {
        f = 0;
        h = 0;

        for (j = 0; j < q_len; j++) {

            n = hearray[j*2 + 0];
            e = hearray[j*2 + 1];
            h += score_matrix[query[j]*32 + db[idx[k].index + i*d]];


            if (e > h)
                h = e;
            if (f > h)
                h = f;
            if (h < 0)
                h = 0;
            if (h > score)
                score = h;

            hearray[j*2 + 0] = h;

            e -= GAPEXTEND;
            f -= GAPEXTEND;
            h -= GAPOPENEXTEND;

            if (h > e)
                e = h;
            if (h > f)
                f = h;

            hearray[j*2 + 1] = e;
            h = n;

        }
    }

#ifdef DEBUG
    printf("%8d | %ld\n",k, score);
#endif

    return score;

}


int pathfinder (struct sequence *q, struct match *m, res_t *r, int k) {

    long i, j, idx_i = -1, idx_j = -1, c = 0, found = 1;

    for (i = q->len; found || i > 0; i--){
        for(j = m->len; j > 0 || found; j--) {
            if(m->path[(i*m->len) + j] == m->score){
                found = 0;
                idx_i = i;
                idx_j = j;
            }
        }
    }

    if ( idx_i == -1 || idx_j == -1 )
        printf("Error\n");
    i = idx_i;
    j = idx_j;

    int up, dia, left;
    while (i >= 0 && j >= 0 ){
        if (!i || !j) {
            if((int)q->seq[i] == (int)m->seq[j]) {
                r->sym[c] = sym_map[(int)q->seq[i]];
            } else {
                r->sym[c] = ' ';
            }

            r->q[c] = sym_map[(int)q->seq[i]];
            r->m[c] =  sym_map[(int)m->seq[j]];
            c++;
            break;
        }
        dia = m->path[(i)*m->len + j];
        left = m->path[(i)*m->len + j-1];
        up = m->path[(i-1)*m->len + j];
        if (!dia && !left && !up)
            break;

        if (dia >= left && dia >= up) {
            if((int)q->seq[i] == (int)m->seq[j]) {
                r->sym[c] = sym_map[(int)q->seq[i]];
            } else {
                r->sym[c] = ' ';
            }

            r->q[c] = sym_map[(int)q->seq[i]];
            r->m[c] =  sym_map[(int)m->seq[j]];
            c++;
            i--;
            j--;
        } else if (up >= dia && up >= left) {
            r->q[c] = sym_map[(int)q->seq[i]];
            r->m[c] = '-';
            r->sym[c] = '+';
            c++;
            i--;
        } else if (left >= dia && left >= up) {
            r->m[c] = sym_map[(int)m->seq[j]];
            r->q[c] = '-';
            r->sym[c] = '-';
            c++;
            j--;
        }

    }

    r->q[c] = '\0';
    r->m[c] = '\0';
    r->sym[c] = '\0';
    return 0;

}
