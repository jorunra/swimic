/* 
 * File:   align.c
 * Author: Jorun Ramstad
 *
 * Created on July 26, 2015, 5:11 PM
 */

#include "common.h"

#ifndef PHI_COMPILED

#include "emmintrin.h"
#include "smmintrin.h"
#define SW_MULTIPLE 4
#define __m512i __m128i
#define _mmx_add_epi32 _mm_add_epi32
#define _mmx_sub_epi32 _mm_sub_epi32
#define _mmx_max_epi32 _mm_max_epi32
int __attribute__((aligned(64))) zero[SW_MULTIPLE] = {0,0,0,0};
int __attribute__((aligned(64))) GOE[SW_MULTIPLE] = {GAPOPENEXTEND, GAPOPENEXTEND, GAPOPENEXTEND, GAPOPENEXTEND};
int __attribute__((aligned(64))) GE[SW_MULTIPLE] = {GAPEXTEND, GAPEXTEND, GAPEXTEND, GAPEXTEND};

#else

#include "immintrin.h"
#include "zmmintrin.h"
#define SW_MULTIPLE 16
#define _mmx_add_epi32 _mm512_add_epi32
#define _mmx_sub_epi32 _mm512_sub_epi32
#define _mmx_max_epi32 _mm512_max_epi32

int __attribute__((aligned(64))) zero[SW_MULTIPLE] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
int __attribute__((aligned(64))) GOE[SW_MULTIPLE] = {GAPOPENEXTEND, GAPOPENEXTEND, GAPOPENEXTEND, GAPOPENEXTEND, GAPOPENEXTEND, GAPOPENEXTEND, GAPOPENEXTEND, GAPOPENEXTEND, GAPOPENEXTEND, GAPOPENEXTEND, GAPOPENEXTEND, GAPOPENEXTEND, GAPOPENEXTEND, GAPOPENEXTEND, GAPOPENEXTEND, GAPOPENEXTEND};
int __attribute__((aligned(64))) GE[SW_MULTIPLE] = {GAPEXTEND, GAPEXTEND, GAPEXTEND, GAPEXTEND, GAPEXTEND, GAPEXTEND, GAPEXTEND, GAPEXTEND, GAPEXTEND, GAPEXTEND, GAPEXTEND, GAPEXTEND, GAPEXTEND, GAPEXTEND, GAPEXTEND, GAPEXTEND};
#endif

#define ALIGN(H, N, E, F, SM, S)                                        \
H = _mmx_add_epi32(H,SM);             /* add comparability score to H */\
H = _mmx_max_epi32(H,F);              /* MAX (H, F)                   */\
H = _mmx_max_epi32(H,E);              /* MAX (H, E)                   */\
H = _mmx_max_epi32(H,*(__m512i*)zero);/* Make sure H > 0              */\
S = _mmx_max_epi32(H,S);              /* Save max score               */\
N = H;                                /* Save H for next step         */\
H = _mmx_sub_epi32(H,*(__m512i*)GOE); /* SUB gap open-extend          */\
F = _mmx_sub_epi32(F,*(__m512i*)GE);  /* SUB gap extend               */\
E = _mmx_sub_epi32(E,*(__m512i*)GE);  /* SUB gap extend               */\
F = _mmx_max_epi32(H,F);              /* Test for opening or extend   */\
E = _mmx_max_epi32(H,E);              /* Teat for opening or extend   */


void SIMD_ALIGN_1(__m512i *HE_array, char *query, int q_len, char *db, idx_t *idx, int k, int16_t *score_matrix) {
    __m512i H, F, N, S, SM;
    int i, j = 0, t;

    int __attribute__((aligned(64))) tmp[SW_MULTIPLE];
    memset(HE_array, 0, 2*q_len*sizeof(__m512i));
    S = *(__m512i*)zero;
    N = HE_array[0];

    for (i = 0; i < idx[k].length; ++i) {
        H = *(__m512i*)zero;
        F = *(__m512i*)zero;

        for (j = 0; j < q_len; ++j) {
            N = HE_array[j*2 + 0];


            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int) score_matrix[query[j]*32 + db[idx[k].index + t + i*distribution]];
            }
            SM = *(__m512i*)tmp;
            ALIGN (H, HE_array[j*2 + 0], HE_array[j*2 + 1], F, SM, S)
            H = N;
        }
    }


    for (i = 0; i < SW_MULTIPLE; ++i){
        idx[k + i].score = ((int *)&S)[i];
    }
}

void SIMD_ALIGN_2 (__m512i *HE_array, char *query, int q_len, char *db, idx_t *idx, int k, int16_t *score_matrix) {
    __m512i E, N, S, SM;
    __m512i H0, H1, H5, H6;
    __m512i F0, F1;
    int i, j, t;
    int __attribute__((aligned(64))) tmp[SW_MULTIPLE];

    memset(HE_array, 0, 2*q_len*sizeof(__m512i));
    H5 = *(__m512i*)zero;
    H6 = *(__m512i*)zero;
    S = *(__m512i*)zero;


    for (i = 0; i < idx[k].length; i+=2) {
        H0 = *(__m512i*)zero;
        H1 = *(__m512i*)zero;
        F0 = *(__m512i*)zero;
        F1 = *(__m512i*)zero;


        for (j = 0; j < q_len; j++) {

            N = HE_array[j*2 + 0];
            E = HE_array[j*2 + 1];


            for (t = 0; t < SW_MULTIPLE; t++) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + t + (i*distribution)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H0, H5, E, F0, SM, S);


            for (t = 0; t < SW_MULTIPLE; t++) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + t + ((i+1)*distribution)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H1, H6, E, F1, SM, S);

            HE_array[j*2 + 0] = H6;
            HE_array[j*2 + 1] = E;

            H0 = N;
            H1 = H5;
        }
    }

    if (idx[k].length % 2 != 0) {
        i = idx[k].length - 2;
        H0 = *(__m512i*)zero;
        F0 = *(__m512i*)zero;

        for (j = 0; j < q_len; j++) {

            N = HE_array[j*2 + 0];
            E = HE_array[j*2 + 1];


            for (t = 0; t < SW_MULTIPLE; t++) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + t + (i*distribution)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H0, HE_array[j*2 + 0], E, F0, SM, S);

            HE_array[j*2 + 1] = E;

            H0 = N;
        }
    }


    for (i = 0; i < SW_MULTIPLE; i++){
        idx[k + i].score = ((int *)&S)[i];
    }
}


void SIMD_ALIGN_4 (__m512i *HE_array, char *query, int q_len, char *db, idx_t *idx, int k, int16_t *score_matrix) {
    __m512i E, S, SM;

    __m512i H0, H1, H2, H3;
    __m512i F0, F1, F2, F3;
    __m512i N0, N1, N2, N3, N4;

    int i, j, t;
    int __attribute__((aligned(64))) tmp[SW_MULTIPLE];

    memset(HE_array, 0, 2*q_len*sizeof(__m512i));
    S = *(__m512i*)zero;
    N0 = *(__m512i*)zero;
    N1 = *(__m512i*)zero;
    N2 = *(__m512i*)zero;
    N3 = *(__m512i*)zero;
    N4 = *(__m512i*)zero;


    for (i = 0; i < idx[k].length; i+=4) {
        H0 = *(__m512i*)zero;
        H1 = *(__m512i*)zero;
        H2 = *(__m512i*)zero;
        H3 = *(__m512i*)zero;
        F0 = *(__m512i*)zero;
        F1 = *(__m512i*)zero;
        F2 = *(__m512i*)zero;
        F3 = *(__m512i*)zero;


        for (j = 0; j < q_len; j++) {

            N0 = HE_array[j*2 + 0];
            E = HE_array[j*2 + 1];

            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + ((i*distribution) + t)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H0, N1, E, F0, SM, S);


            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + (((i+1)*distribution) + t)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H1, N2, E, F1, SM, S);


            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + (((i+2)*distribution) + t)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H2, N3, E, F2, SM, S);


            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + (((i+3)*distribution) + t)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H3, N4, E, F3, SM, S);

            HE_array[j*2 + 0] = N4;
            HE_array[j*2 + 1] = E;

            H0 = N0;
            H1 = N1;
            H2 = N2;
            H3 = N3;

        }
    }

    int r = idx[k].length % 4;
    int l = idx[k].length - r;

    for (i = l; i < idx[k].length; i++) {
        H0 = *(__m512i*)zero;
        F0 = *(__m512i*)zero;

        for (j = 0; j < q_len; j++) {

            N0 = HE_array[j*2 + 0];

            for (t = 0; t < SW_MULTIPLE; t++) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + t + (i*distribution)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H0, HE_array[j*2 + 0], HE_array[j*2 + 1], F0, SM, S);

            H0 = N0;
        }
    }

    for (i = 0; i < SW_MULTIPLE; i++){
        idx[k + i].score = ((int *)&S)[i];
    }
}


void SIMD_ALIGN_6 (__m512i *HE_array, char *query, int q_len, char *db, idx_t *idx, int k, int16_t *score_matrix) {
    __m512i E, S, SM;
    __m512i H0, H1, H2, H3, H4, H5;
    __m512i F0, F1, F2, F3, F4, F5;
    __m512i N0, N1, N2, N3, N4, N5, N6;
    int i, j, t;
    int __attribute__((aligned(64))) tmp[SW_MULTIPLE];

    memset(HE_array, 0, 2*q_len*sizeof(__m512i));
    S = *(__m512i*)zero;
    N0 = *(__m512i*)zero;
    N1 = *(__m512i*)zero;
    N2 = *(__m512i*)zero;
    N3 = *(__m512i*)zero;
    N4 = *(__m512i*)zero;
    N5 = *(__m512i*)zero;
    N6 = *(__m512i*)zero;


    for (i = 0; i < idx[k].length; i+=6) {
        H0 = *(__m512i*)zero;
        H1 = *(__m512i*)zero;
        H2 = *(__m512i*)zero;
        H3 = *(__m512i*)zero;
        H4 = *(__m512i*)zero;
        H5 = *(__m512i*)zero;

        F0 = *(__m512i*)zero;
        F1 = *(__m512i*)zero;
        F2 = *(__m512i*)zero;
        F3 = *(__m512i*)zero;
        F4 = *(__m512i*)zero;
        F5 = *(__m512i*)zero;


        for (j = 0; j < q_len; j++) {

            N0 = HE_array[j*2 + 0];
            E = HE_array[j*2 + 1];


            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + ((i*distribution) + t)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H0, N1, E, F0, SM, S);


            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + (((i+1)*distribution) + t)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H1, N2, E, F1, SM, S);


            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + (((i+2)*distribution) + t)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H2, N3, E, F2, SM, S);


            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + (((i+3)*distribution) + t)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H3, N4, E, F3, SM, S);


            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + (((i+4)*distribution) + t)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H4, N5, E, F4, SM, S);


            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + (((i+5)*distribution) + t)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H5, N6, E, F5, SM, S);

            HE_array[j*2 + 0] = N6;
            HE_array[j*2 + 1] = E;

            H0 = N0;
            H1 = N1;
            H2 = N2;
            H3 = N3;
            H4 = N4;
            H5 = N5;
        }
    }

    int r = idx[k].length % 6;
    int l = idx[k].length - r;

    for (i = l; i < idx[k].length; i++) {
        H0 = *(__m512i*)zero;
        F0 = *(__m512i*)zero;

        for (j = 0; j < q_len; j++) {

            N0 = HE_array[j*2 + 0];


            for (t = 0; t < SW_MULTIPLE; t++) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + t + (i*distribution)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H0, HE_array[j*2 + 0], HE_array[j*2 + 1], F0, SM, S);

            H0 = N0;
        }
    }


    for (i = 0; i < SW_MULTIPLE; ++i){
        idx[k + i].score = ((int *)&S)[i];
    }
}


void SIMD_ALIGN_8 (__m512i *HE_array, char *query, int q_len, char *db, idx_t *idx, int k, int16_t *score_matrix) {
    __m512i S, SM;
    __m512i H0, H1, H2, H3, H4, H5, H6, H7;
    __m512i F0, F1, F2, F3, F4, F5, F6, F7;
    __m512i N0, N1, N2, N3, N4, N5, N6, N7, N8;
    int i, j, t;
    int __attribute__((aligned(64))) tmp[SW_MULTIPLE];

    memset(HE_array, 0, 2*q_len*sizeof(__m512i));
    S = *(__m512i*)zero;
    N0 = *(__m512i*)zero;
    N1 = *(__m512i*)zero;
    N2 = *(__m512i*)zero;
    N3 = *(__m512i*)zero;
    N4 = *(__m512i*)zero;
    N5 = *(__m512i*)zero;
    N6 = *(__m512i*)zero;
    N7 = *(__m512i*)zero;
    N8 = *(__m512i*)zero;


    for (i = 0; i < idx[k].length; i+=8) {
        H0 = *(__m512i*)zero;
        H1 = *(__m512i*)zero;
        H2 = *(__m512i*)zero;
        H3 = *(__m512i*)zero;
        H4 = *(__m512i*)zero;
        H5 = *(__m512i*)zero;
        H6 = *(__m512i*)zero;
        H7 = *(__m512i*)zero;

        F0 = *(__m512i*)zero;
        F1 = *(__m512i*)zero;
        F2 = *(__m512i*)zero;
        F3 = *(__m512i*)zero;
        F4 = *(__m512i*)zero;
        F5 = *(__m512i*)zero;
        F6 = *(__m512i*)zero;
        F7 = *(__m512i*)zero;


        for (j = 0; j < q_len; ++j) {

            N0 = HE_array[j*2 + 0];

            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + ((i*distribution) + t)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H0, N1, HE_array[j*2 + 1], F0, SM, S);

            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + (((i+1)*distribution) + t)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H1, N2, HE_array[j*2 + 1], F1, SM, S);

            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + (((i+2)*distribution) + t)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H2, N3, HE_array[j*2 + 1], F2, SM, S);


            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + (((i+3)*distribution) + t)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H3, N4, HE_array[j*2 + 1], F3, SM, S);

            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + (((i+4)*distribution) + t)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H4, N5, HE_array[j*2 + 1], F4, SM, S);


            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + (((i+5)*distribution) + t)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H5, N6, HE_array[j*2 + 1], F5, SM, S);


            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + (((i+6)*distribution) + t)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H6, N7, HE_array[j*2 + 1], F6, SM, S);


            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + (((i+7)*distribution) + t)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H7, N8, HE_array[j*2 + 1], F7, SM, S);

            HE_array[j*2 + 0] = N8;

            H0 = N0;
            H1 = N1;
            H2 = N2;
            H3 = N3;
            H4 = N4;
            H5 = N5;
            H6 = N6;
            H7 = N7;
        }
    }

    int r = idx[k].length % 8;
    int l = idx[k].length - r;

    for (i = l; i < idx[k].length; i++) {
        H0 = *(__m512i*)zero;
        F0 = *(__m512i*)zero;


        for (j = 0; j < q_len; j++) {

            N0 = HE_array[j*2 + 0];

            for (t = 0; t < SW_MULTIPLE; t++) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + t + (i*distribution)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H0, HE_array[j*2 + 0], HE_array[j*2 + 1], F0, SM, S);

            H0 = N0;
        }
    }

    for (i = 0; i < SW_MULTIPLE; ++i){
        idx[k + i].score = ((int *)&S)[i];
    }
}

void SIMD_ALIGN_10 (__m512i *HE_array, char *query, int q_len, char *db, idx_t *idx, int k, int16_t *score_matrix) {
    __m512i S, SM, E;
    __m512i H0, H1, H2, H3, H4, H5, H6, H7, H8, H9;
    __m512i F0, F1, F2, F3, F4, F5, F6, F7, F8, F9;
    __m512i N0, N1, N2, N3, N4, N5, N6, N7, N8, N9, N10;
    int i, j, t;
    int __attribute__((aligned(64))) tmp[SW_MULTIPLE];

    memset(HE_array, 0, 2*q_len*sizeof(__m512i));
    S = *(__m512i*)zero;
    N0 = *(__m512i*)zero;
    N1 = *(__m512i*)zero;
    N2 = *(__m512i*)zero;
    N3 = *(__m512i*)zero;
    N4 = *(__m512i*)zero;
    N5 = *(__m512i*)zero;
    N6 = *(__m512i*)zero;
    N7 = *(__m512i*)zero;
    N8 = *(__m512i*)zero;
    N9 = *(__m512i*)zero;
    N10 = *(__m512i*)zero;

#ifdef PHI_COMPILED
#pragma vector aligned
#endif
    for (i = 0; i < idx[k].length; i+=10) {
        H0 = *(__m512i*)zero;
        H1 = *(__m512i*)zero;
        H2 = *(__m512i*)zero;
        H3 = *(__m512i*)zero;
        H4 = *(__m512i*)zero;
        H5 = *(__m512i*)zero;
        H6 = *(__m512i*)zero;
        H7 = *(__m512i*)zero;
        H8 = *(__m512i*)zero;
        H9 = *(__m512i*)zero;

        F0 = *(__m512i*)zero;
        F1 = *(__m512i*)zero;
        F2 = *(__m512i*)zero;
        F3 = *(__m512i*)zero;
        F4 = *(__m512i*)zero;
        F5 = *(__m512i*)zero;
        F6 = *(__m512i*)zero;
        F7 = *(__m512i*)zero;
        F8 = *(__m512i*)zero;
        F9 = *(__m512i*)zero;
#ifdef PHI_COMPILED
#pragma vector aligned
#endif
        for (j = 0; j < q_len; ++j) {

            N0 = HE_array[j*2 + 0];
            E = HE_array[j*2 + 1];

#ifdef PHI_COMPILED
#pragma vector aligned
#endif
            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + ((i*distribution) + t)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H0, N1, E, F0, SM, S);
#ifdef PHI_COMPILED
#pragma vector aligned
#endif
            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + (((i+1)*distribution) + t)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H1, N2, E, F1, SM, S);
#ifdef PHI_COMPILED
#pragma vector aligned
#endif
            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + (((i+2)*distribution) + t)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H2, N3, E, F2, SM, S);

#ifdef PHI_COMPILED
#pragma vector aligned
#endif
            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + (((i+3)*distribution) + t)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H3, N4, E, F3, SM, S);
#ifdef PHI_COMPILED
#pragma vector aligned
#endif
            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + (((i+4)*distribution) + t)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H4, N5, E, F4, SM, S);

#ifdef PHI_COMPILED
#pragma vector aligned
#endif
            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + (((i+5)*distribution) + t)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H5, N6, E, F5, SM, S);

#ifdef PHI_COMPILED
#pragma vector aligned
#endif
            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + (((i+6)*distribution) + t)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H6, N7, E, F6, SM, S);

#ifdef PHI_COMPILED
#pragma vector aligned
#endif
            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + (((i+7)*distribution) + t)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H7, N8, E, F7, SM, S);

#ifdef PHI_COMPILED
#pragma vector aligned
#endif
            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + (((i+8)*distribution) + t)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H8, N9, E, F8, SM, S);

#ifdef PHI_COMPILED
#pragma vector aligned
#endif
            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + (((i+9)*distribution) + t)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H9, N10, E, F9, SM, S);

            HE_array[j*2 + 0] = N10;
            HE_array[j*2 + 1] = E;

            H0 = N0;
            H1 = N1;
            H2 = N2;
            H3 = N3;
            H4 = N4;
            H5 = N5;
            H6 = N6;
            H7 = N7;
            H8 = N8;
            H9 = N9;

        }
    }

    int r = idx[k].length % 10;
    int l = idx[k].length - r;

#ifdef PHI_COMPILED
#pragma vector aligned
#endif
    for (i = l; i < idx[k].length; i++) {
        H0 = *(__m512i*)zero;
        F0 = *(__m512i*)zero;

#ifdef PHI_COMPILED
#pragma vector aligned
#endif
        for (j = 0; j < q_len; j++) {

            N0 = HE_array[j*2 + 0];
            E = HE_array[j*2 + 1];

#ifdef PHI_COMPILED
#pragma vector aligned
#endif
            for (t = 0; t < SW_MULTIPLE; t++) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + t + (i*distribution)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H0, HE_array[j*2 + 0], E, F0, SM, S);

            H0 = N0;
            HE_array[j*2 + 1] = E;
        }
    }

#ifdef PHI_COMPILED
#pragma vector aligned
#endif
    for (i = 0; i < SW_MULTIPLE; i++){
        idx[k + i].score = ((int *)&S)[i];
    }
}


void SIMD_ALIGN_12 (__m512i *HE_array, char *query, int q_len, char *db, idx_t *idx, int k, int16_t *score_matrix) {
    __m512i E, S, SM;
    __m512i H0, H1, H2, H3, H4, H5, H6, H7, H8, H9, H10, H11;
    __m512i F0, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11;
    __m512i N0, N1, N2, N3, N4, N5, N6, N7, N8, N9, N10, N11, N12;
    int i, j, t;
    int __attribute__((aligned(64))) tmp[SW_MULTIPLE];

    memset(HE_array, 0, 2*q_len*sizeof(__m512i));
    S = *(__m512i*)zero;
    N0 = *(__m512i*)zero;
    N1 = *(__m512i*)zero;
    N2 = *(__m512i*)zero;
    N3 = *(__m512i*)zero;
    N4 = *(__m512i*)zero;
    N5 = *(__m512i*)zero;
    N6 = *(__m512i*)zero;
    N7 = *(__m512i*)zero;
    N8 = *(__m512i*)zero;
    N9 = *(__m512i*)zero;
    N10 = *(__m512i*)zero;
    N11 = *(__m512i*)zero;
    N12 = *(__m512i*)zero;


    for (i = 0; i < idx[k].length; i+=12) {
        H0 = *(__m512i*)zero;
        H1 = *(__m512i*)zero;
        H2 = *(__m512i*)zero;
        H3 = *(__m512i*)zero;
        H4 = *(__m512i*)zero;
        H5 = *(__m512i*)zero;
        H6 = *(__m512i*)zero;
        H7 = *(__m512i*)zero;
        H8 = *(__m512i*)zero;
        H9 = *(__m512i*)zero;
        H10 = *(__m512i*)zero;
        H11 = *(__m512i*)zero;

        F0 = *(__m512i*)zero;
        F1 = *(__m512i*)zero;
        F2 = *(__m512i*)zero;
        F3 = *(__m512i*)zero;
        F4 = *(__m512i*)zero;
        F5 = *(__m512i*)zero;
        F6 = *(__m512i*)zero;
        F7 = *(__m512i*)zero;
        F8 = *(__m512i*)zero;
        F9 = *(__m512i*)zero;
        F10 = *(__m512i*)zero;
        F11 = *(__m512i*)zero;


        for (j = 0; j < q_len; ++j) {

            N0 = HE_array[j*2 + 0];
            E = HE_array[j*2 + 1];


            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + t + (i*distribution)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H0, N1, E, F0, SM, S);


            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + t + ((i+1)*distribution)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H1, N2, E, F1, SM, S);


            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + t + ((i+2)*distribution)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H2, N3, E, F2, SM, S);


            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + t + ((i+3)*distribution)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H3, N4, E, F3, SM, S);


            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + t + ((i+4)*distribution)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H4, N5, E, F4, SM, S);


            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + t + ((i+5)*distribution)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H5, N6, E, F5, SM, S);


            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + t + ((i+6)*distribution)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H6, N7, E, F6, SM, S);


            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + t + ((i+7)*distribution)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H7, N8, E, F7, SM, S);


            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + t + ((i+8)*distribution)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H8, N9, E, F8, SM, S);


            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + t + ((i+9)*distribution)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H9, N10, E, F9, SM, S);


            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + t + ((i+10)*distribution)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H10, N11, E, F10, SM, S);


            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + t + ((i+11)*distribution)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H11, N12, E, F11, SM, S);


            HE_array[j*2 + 0] = N12;
            HE_array[j*2 + 1] = E;

            H0 = N0;
            H1 = N1;
            H2 = N2;
            H3 = N3;
            H4 = N4;
            H5 = N5;
            H6 = N6;
            H7 = N7;
            H8 = N8;
            H9 = N9;
            H10 = N10;
            H11 = N11;
        }
    }

    int r = idx[k].length % 12;
    int l = idx[k].length - r;

    for (i = l; i < idx[k].length; i++) {
        H0 = *(__m512i*)zero;
        F0 = *(__m512i*)zero;

        for (j = 0; j < q_len; ++j) {

            N0 = HE_array[j*2 + 0];

            for (t = 0; t < SW_MULTIPLE; ++t) {
                tmp[t] = (int)score_matrix[query[j]*32 + db[idx[k].index + t + (i*distribution)]];
            }
            SM = *(__m512i*)tmp;
            ALIGN(H0, HE_array[j*2 + 0], HE_array[j*2 + 1], F0, SM, S);

            H0 = N0;
        }
    }

    for (i = 0; i < SW_MULTIPLE; i++){
        idx[k + i].score = ((int *)&S)[i];
    }
}


int SIMD_ALIGN (int range, char *query, int q_len, char *db, idx_t *idx, int16_t *score_matrix, int seq_total) {
    int k;
    __m512i *HE_array;

    int size = 2*q_len*sizeof(__m512i);
    switch (range) {
        case 2:
#pragma omp parallel private(k, HE_array)
            {
                HE_array = (__m512i *)_mm_malloc(size,64);
#ifdef PHI_COMPILED
#pragma vector aligned
#endif
#pragma omp for schedule(dynamic)
                for (k = 0; k < seq_total; k += SW_MULTIPLE) {
                    SIMD_ALIGN_2(HE_array, query, q_len, db, idx, k, score_matrix);
                }
                _mm_free(HE_array);
            }
            break;
        case 4:
#pragma omp parallel private(k, HE_array)
            {
                HE_array = (__m512i *)_mm_malloc(size,64);

#ifdef PHI_COMPILED
#pragma vector aligned
#endif
#pragma omp for schedule(dynamic)
                for (k = 0; k < seq_total; k += SW_MULTIPLE) {
                    SIMD_ALIGN_4(HE_array, query, q_len, db, idx, k, score_matrix);
                }
                _mm_free(HE_array);
            }
            break;
        case 6:
#pragma omp parallel private(k, HE_array)
            {
                HE_array = (__m512i *)_mm_malloc(size,64);

#ifdef PHI_COMPILED
#pragma vector aligned
#endif
#pragma omp for schedule(dynamic)
                for (k = 0; k < seq_total; k += SW_MULTIPLE) {
                    SIMD_ALIGN_6(HE_array, query, q_len, db, idx, k, score_matrix);
                }
                _mm_free(HE_array);
            }
            break;

        case 8:
#pragma omp parallel private(HE_array)
            {
                HE_array = (__m512i *)_mm_malloc(size,64);

#ifdef PHI_COMPILED
#pragma vector aligned
#endif
#pragma omp for schedule(dynamic)
                for (k = 0; k < seq_total; k += SW_MULTIPLE) {
                    SIMD_ALIGN_8(HE_array, query, q_len, db, idx, k, score_matrix);
                }
                _mm_free(HE_array);
            }
            break;
        case 10:
#pragma omp parallel private(HE_array)
            {
                HE_array = (__m512i *)_mm_malloc(size,64);
#ifdef PHI_COMPILED
#pragma vector aligned
#endif
#pragma omp for schedule(dynamic)
                for (k = 0; k < seq_total; k += SW_MULTIPLE) {
                    SIMD_ALIGN_10(HE_array, query, q_len, db, idx, k, score_matrix);
                }
                _mm_free(HE_array);
            }
            break;
        case 12:
#pragma omp parallel private(HE_array)
            {
                HE_array = (__m512i *)_mm_malloc(size,64);

#ifdef PHI_COMPILED
#pragma vector aligned
#endif
#pragma omp for schedule(dynamic)
                for (k = 0; k < seq_total; k += SW_MULTIPLE) {
                    SIMD_ALIGN_12(HE_array, query, q_len, db, idx, k, score_matrix);
                }
                _mm_free(HE_array);
            }
            break;


        default:
#pragma omp parallel private(k, HE_array)
            {
                HE_array = (__m512i *)_mm_malloc(size ,64);

#ifdef PHI_COMPILED
#pragma vector aligned
#endif
#pragma omp for schedule(dynamic)
                for (k = 0; k < seq_total; k += SW_MULTIPLE) {
                    SIMD_ALIGN_1(HE_array, query, q_len, db, idx, k, score_matrix);
                }
                _mm_free(HE_array);
            }
            break;
    }
    return seq_total % SW_MULTIPLE;
}
