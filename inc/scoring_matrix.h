/* 
 * File:   scoring_matrix.h
 * Author: Jorun Ramstad
 *
 * Created on July 26, 2015, 5:09 PM
 */

#ifndef SCORING_MATRIX_H
#define	SCORING_MATRIX_H

int16_t __attribute__((aligned(64))) score_matrix[32*32];
//int16_t *score_matrix;


/**
 * Prints the currently initialized scoring matrix to the specified output file.
 */
void matrix_print();

/**
 * Reads a scoring matrix from a file.
 *
 * @param matrix    name of the file containing the scoring matrix
 */
void matrix_init_from_file(const char * matrix);

/**
 * Reads a scoring matrix from a string.
 *
 * @param matrix    scoring matrix as a string
 */
void matrix_init_from_string(const char * matrix);

/**
 * Prepares one of the build-in scoring matrixes.
 *
 * List of available matrices:
 *  - blosum45
 *  - blosum50
 *  - blosum62
 *  - blosum80
 *  - blosum90
 *  - pam30
 *  - pam70
 *  - pam250
 *
 * @param matrixname    one of the available matrices
 */
void matrix_init_buildin(const char* matrixname);

/**
 * Releases the memory allocated for the scoring matrices.
 */
void matrix_free();

#endif	/* SCORING_MATRIX_H */

