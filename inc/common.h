/* 
 * File:   common.h
 * Author: Jorun Ramstad
 *
 * Created on July 26, 2015, 5:09 PM
 */

#ifndef COMMON_H
#define COMMON_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LINE_MAX 2048
#define DES_LEN 30 // description length
//#define THREADS 240
//#define MAX_STORE 20       // How many best matches do we want to keep? XXX: MUST BE LARGER THAN 1
//#define FULL_SW (THREADS > MAX_STORE) ? MAX_STORE : THREADS

#define BLOSUM45 "blosum45"
#define BLOSUM50 "blosum50"
#define BLOSUM62 "blosum62"
#define BLOSUM80 "blosum80"
#define BLOSUM90 "blosum90"
#define PAM30 "pam30"
#define PAM70 "pam70"
#define PAM250 "pam250"

#define GAPOPENEXTEND 12
#define GAPEXTEND 1

int threads;
int distribution;
int max_store;


struct sequence {
    char *seq;
    long len;
};

struct match {
    long score;
    long *path;
    char *seq;
    long len;
};

typedef struct res {
    char *q;
    char *m;
    char *sym;
}res_t;

typedef struct storage {
    int score;
    long db_index;
    int s_idx;
    struct storage *next;
    struct storage *prev;
} storage_t;

typedef struct idx {
    unsigned int index;
    int score;
    int length;
    char description[DES_LEN];
}idx_t;


/*
 * Maps amino acids to their numerical representation. Maps upper case and lower
 * case letters.
 *
 * The supported symbols are:
 * 0                          27
 * -ABCDEFGHIKLMNPQRSTVWXYZU*OJ
 */
extern const char map[256];


/*
 * Maps the numerical representation of amino acids back to symbols in upper case.
 * Maps 28 codes for amino acids:
 *
 * -ABCDEFGHIKLMNPQRSTVWXYZU*OJ
 */
extern const char * sym_map;



int SIMD_ALIGN(int range, char *query, int q_len, char *db, idx_t *idx, int16_t *score_matrix, int seq_total);


long smith_waterman_distibuted (struct match *m, char *query, int q_len, char *db, idx_t *idx, int k,  int16_t *score_matrix, int d);
int pathfinder_ditributed (struct sequence *q, struct match *m, res_t *r, int k);

/**
 * Does a full Smith-Waterman search and return the highest score calculated
 **/
long fullsw(char * dseq, char * dend, char * qseq, char * qend, long *hearray, int16_t *score_matrix, int gapopenextend, int gapextend);
long smith_waterman (struct sequence *q, struct match *m,  int16_t *score_matrix, int gapopenextend, int gapextend);
long sw (long *hearray, char *query, int q_len, char *db, idx_t *idx, int k,  int16_t *score_matrix, int d);

/**
 * Backtracks the H array an composes the alignment
 **/
int pathfinder (struct sequence *q, struct match *m, res_t *r, int k);

/**
 * Reads from file
 **/

long read_pdb(FILE *file, char **db, idx_t **idx, long *residues);
long read_distributed_pdb(FILE *file, char **db, idx_t **idx, long *residues, long *len_total);
int read_query (struct sequence *s, FILE *file); // read.c

/**
 * Adds a new storage struct to the list of stored. Will always update the
 * lowest pointer and returns a pointer to the highest stored value.
 * The list will be sorted according to highest aligning score.
 *
 * @param storage_space: pointer to a malloced area of size MAX_STORE*sizeof(storage_t)
 * @param high: pointer to the previously highest stored entry in storage_space.
 * @param low: Same as high, but the lowest instead. (Note: this is the address of the pointer and not the struct itself).
 * @param storage_idx: pointer to an int that should have been initialized to 0. (no need to worry about this int later).
 * @param score: the score of the new match.
 * @param db_index: database index in idx.
 **/
storage_t *add(storage_t *storage_space, storage_t *high, storage_t **low, int *storage_idx, int score, long db_index);

/**
 * Converts a sequence back to amino acids and prints the sequence to screen
 **/
void print_sequence (struct sequence *s);
void write_sequence(struct sequence *s, FILE * file);
void print_seq(char *seq, int len);
void print_matches(storage_t *cur, idx_t *idx);
void write_matches(res_t *r, storage_t *high, FILE *file, idx_t *idx, int num);

#endif /* COMMON_H */

