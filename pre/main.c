#include "prepare.h"
int main (int argc, char *argv[]) {

    if (argc == 1) {
        return -1;
    }

    FILE *db_file = fopen(argv[1],"r");

    char *db = (char *) malloc((long)INIT_DB_LEN);

    node_t *root = (node_t *) malloc(sizeof(node_t));
    root->seq = NULL;
    root->len = 0;
    root->left = NULL;
    root->right = NULL;

    long seq_total;
    long len = read_fasta(&db, db_file, root, &seq_total);
    printf("total amount of sequences: %ld\n", seq_total);
    printf("total length of all sequences: %ld\n", len);

    fclose(db_file);

    db = (char *) realloc(db, len-1);
    // print_tree(root);
    //int pad_len = 0;
    long count = 0;
    long len_total = 0;
    int distribution = 16;
    int i;

    char *filename = malloc(30);
    sprintf(filename, "%s_padded_%d_aligned.pdb", argv[1], distribution);
    printf("%s\n", filename);


    db_file = fopen(filename, "wb");

    fwrite((void *)&len_total, sizeof(long), 1, db_file);
    fwrite((void *)&len, sizeof(long), 1, db_file);
    fwrite((void *)&seq_total, sizeof(long), 1, db_file);
    fwrite((void *)&distribution, sizeof(int), 1, db_file);

    //write_tree(root, db_file, &pad_len, &count, &len_total);

    node_t **nodes = (node_t **)malloc(distribution*sizeof(node_t *));
    distribute_tree(root, nodes, db_file, &count, &len_total, distribution);



    for (i = 0; i < count; i++) {
        len_total += nodes[i]->len;
        fwrite((void *)&nodes[i]->len, sizeof(int), 1, db_file);
        fwrite((void *)nodes[i]->seq, sizeof(char), nodes[i]->len, db_file);
        fwrite((void *)&nodes[i]->des, sizeof(char), DES_LEN, db_file);
    }

    fseek(db_file, 0, SEEK_SET);
    fwrite((void *)&len_total, sizeof(long), 1, db_file);

    fclose(db_file);
    free_tree(root);
    free(db);
    free(nodes);

    /*
    db_file = fopen(filename, "rb");

    char *new_db;

    long l = 0;
    idx_t *idx = NULL;
    read_distributed_pdb(db_file, &new_db, &idx, &l);

    fclose(db_file);

    free(new_db);
    free(idx);
    */

    return 0;
}
