#ifndef PREPARE
#define PREPARE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LINE_MAX 2048
#define INIT_DB_LEN 3*1024*1024*1024
#define DB_EXTENTION 2048
#define SEQ_CHUNCK 32
#define DES_LEN 30


typedef struct node {
    int len;
    char *seq;
    char des[DES_LEN];
    struct node *left, *right;
} node_t;

/*
 * Maps amino acids to their numerical representation. Maps upper case and lower
 * case letters.
 *
 * The supported symbols are:
 * 0                          27
 * -ABCDEFGHIKLMNPQRSTVWXYZU*OJ
 */
extern const char map[256];


/*
 * Maps the numerical representation of amino acids back to symbols in upper case.
 * Maps 28 codes for amino acids:
 *
 * -ABCDEFGHIKLMNPQRSTVWXYZU*OJ
 */
extern const char * sym_map;

typedef struct idx {
    long index;
    int length;
    int score;
    char description[DES_LEN];
}idx_t;

long read_fasta (char **db, FILE *file, node_t *root, long *seq_total);
void add(node_t *root, char *seq, int len, char des[DES_LEN]);
void print_sequence (char *s, int len, char *des);
void print_tree (node_t *n);
void write_tree (node_t *n, FILE *file, int *pad_len, long *count, long *len_total);
long read_pdb(FILE *file, char **db, idx_t **idx, long *len_total);
void write_sequence (char *s, int len, char des[DES_LEN], FILE *file, int *pad_len, long *count, long *len_total);
void free_tree (node_t *n);
void distribute_tree (node_t *n, node_t **nodes, FILE *file, long *count, long *len_total, int distribution);
long read_distributed_pdb(FILE *file, char **db, idx_t **idx, long *residues);

#endif
