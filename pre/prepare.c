#include "prepare.h"
const char map[256] = {
-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 25, -1, -1, 0, -1, -1,
-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
-1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 27, 10, 11, 12, 13, 26,
14, 15, 16, 17, 18, 24, 19, 20, 21, 22, 23, -1, -1, -1, -1, -1,
-1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 27, 10, 11, 12, 13, 26,
14, 15, 16, 17, 18, 24, 19, 20, 21, 22, 23, -1, -1, -1, -1, -1,
-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
};

const char * sym_map = "-ABCDEFGHIKLMNPQRSTVWXYZU*OJ####";

// Convert the sequence back to symbols and print it
void print_sequence (char *s, int len, char *des) {
    int c;
    for (c = 0; c < len; c++)
        printf("%c ",sym_map[(int)s[c]]);
    printf("\n");

}

// Adding the new node at the right location in the sorted tree
void adding(node_t *e, node_t *c) {
    if (e->len > c->len) {
        if(e->left == NULL)
            e->left = c;
        else
            adding(e->left, c);
        return;
    }
    if (e->len <= c->len) {
        if (e->right == NULL)
            e->right = c;
        else {
            adding(e->right, c);
        }
        return;
    }
}

// Adding a new node in the tree
void add(node_t *root, char *seq, int len, char *des) {
    if (root->len == 0) {
        root->seq = seq;
        root->len = len;
        strncpy(root->des, des, DES_LEN);
        root->left = NULL;
        root->right = NULL;
        return;
    }

    node_t *n = (node_t *) malloc(sizeof(node_t));
    n->seq = seq;
    n->len = len;
    strncpy(n->des, des, DES_LEN);
    n->left = NULL;
    n->right = NULL;

    adding(root, n);
}

// Recursivly iterating the tree and printing
void print_tree (node_t *n) {
    if(n->left != NULL)
        print_tree(n->left);
    print_sequence(n->seq, n->len, n->des);
    if(n->right != NULL)
        print_tree(n->right);
}

// Write sequence to file with a * (0x19) as padding
void write_sequence (char *s, int len, char des[DES_LEN], FILE *file, int *pad_len, long *count, long *len_total) {
    if ((*count % 16) == 0)
        *pad_len = len;
    fwrite((void *)pad_len, sizeof(int), 1, file);
    fwrite((void *)s, sizeof(char), len, file);

    char p = 0x19;

    int i;
    for (i = len; i < *pad_len; i++)
        fwrite((void *)&p, sizeof(char), 1, file);

    fwrite((void *)des, sizeof(char), DES_LEN, file);
    *count += 1;

    *len_total += (long)*pad_len;
}

// Recursivly iteratiing the tree and call write_sequence in the sorted order
void write_tree (node_t *n, FILE *file, int *pad_len, long *count, long *len_total) {
    if(n->right != NULL)
        write_tree(n->right, file, pad_len, count, len_total);
    write_sequence(n->seq, n->len, n->des, file, pad_len, count, len_total);
    if(n->left != NULL)
        write_tree(n->left, file, pad_len, count, len_total);
}

// Write x database sequences to file with a distibution.
void write_distributed(node_t **nodes, FILE *file, long *len_total, int distribution) {

    int len = (64 - (nodes[0]->len)%64) + nodes[0]->len;
    //printf("%f\n", len/64);

    //printf("%d | %d \n", len, nodes[0]->len);

    fwrite((void *)&len, sizeof(int), 1, file);
    fwrite((void *)&nodes[0]->len, sizeof(int), 1, file);

    int i, k;
    char p = 0x19;

    for (k = 0; k < len; k++) {
        for (i = 0; i < distribution; i++) {
            *len_total += 1;
            if (nodes[i]->len < k)
                fwrite((void *)&p, sizeof(char), 1, file);
            else
                fwrite((void *)&nodes[i]->seq[k], sizeof(char), 1, file);
        }
    }

    for (i = 0; i < distribution; i++) {
        fwrite((void *)&nodes[i]->des, sizeof(char), DES_LEN, file);
    }
}

// Recursivly iteratiing the tree calling write_distributed to write the given
// sequences to file.
void distribute_tree (node_t *n, node_t **nodes, FILE *file, long *count, long *len_total, int distribution) {
    if(n->right != NULL)
        distribute_tree(n->right, nodes, file, count, len_total, distribution);

    nodes[*count] = n;
    *count += 1;

    if (*count == distribution) {
        write_distributed(nodes, file, len_total, distribution);
        *count = 0;
    }

    if(n->left != NULL)
        distribute_tree(n->left, nodes, file, count, len_total, distribution);
}


// Free ut memory for all nodes
void free_tree (node_t *n) {
    if(n->right != NULL)
        free_tree(n->right);
    free(n);
    if(n->left != NULL)
        free_tree(n->left);
}

// Read the database file and add sequences to tree in a converted form
long read_fasta (char **db, FILE *file, node_t *root, long *seq_total) {

    long len = 0;
    long size = (long)INIT_DB_LEN;

    int c, s_len;
    char query[LINE_MAX];
    char des[DES_LEN];

    char *prev = *db;
    int seq_len = 0;
    *seq_total = 0;

    int first = 1;

    while (1) {
        // If the end of the file is reached, the last sequence is saved and the
        // total length is returnd.
        if (fgets(query, LINE_MAX, file) == 0){
            if (!first) {
                add(root, prev, seq_len, des);
                *seq_total += 1;
            }
            return len;
        }

        s_len = strlen(query);

        // If the description is longer than LINE_MAX, the rest of the
        // description is read until a new line is reached.
        if (query[0] == '>') {

            // The last read sequence is saved and variables are reset.
            if (!first) {
                add(root, prev, seq_len, des);
                prev = (char *)&((*db)[len]);
                seq_len = 0;
                *seq_total += 1;

            }
            if ((s_len == LINE_MAX - 1)){
                while (query[s_len - 1] != '\n') {
                    if(fgets(query, LINE_MAX, file) == 0)
                        return len;
                    s_len = strlen(query);
                }
            }
            first = 0;

            // Find the right part of the description
            int i = 0;
            while (1) {
                if (query[i] == 's') {
                    strncpy(des, &query[i], DES_LEN);
                    break;
                }
                i++;
            }

        } else {
            // Realloc if the database is too long.
            if ((long)(len + s_len) > size){
                printf("Realloc\n");
                *db = (char *)realloc(*db, size + DB_EXTENTION);
                size += DB_EXTENTION;
            }

            // Read the sequence and store it in the given sequence
            // struct. Each amino acid is mapped to a number between 0-27.
            char *p = query;
            c = 0;
            while(c < s_len - 1) {
                (*db)[len++] = map[(int) *p++];
                seq_len++;
                c++;
            }
        }
    }
    return len;
}
